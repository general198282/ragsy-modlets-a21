
Required Legal Notice

Hind helicopter Modlet for 7 Days To Die Game.

All Models in this modlet are subject to creative commons licence.
https://creativecommons.org/licenses/by/4.0/

Model Credits : Ashley Aslett

No Model parts have been altered from the 'original creators' they are set up with appropriate lighting shader and textures in unity for 7 Days to Die game.
On this model the rockets and pods removed as we can not shoot missiles and guns in A20 yet. 
A big thanks to all the modellers who release work for free usage !

